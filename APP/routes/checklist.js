const{createAChecklist,getAllChecklists, deleteAChecklist}= require('../controllers/checklistsController')


var routerChecklists = require("express").Router();


routerChecklists.get('/',getAllChecklists);

routerChecklists.post('/',createAChecklist);

routerChecklists.delete('/',deleteAChecklist);

module.exports=routerChecklists;