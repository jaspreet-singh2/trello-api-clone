const { boards } = require(".");

module.exports = (sequelize, Sequelize) => {
    const lists = sequelize.define("lists", {
       
      id: {
        // type: Sequelize.INTEGER,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
        primaryKey: true
      },
      name: {
        type: Sequelize.STRING
      },
     
    //   boardId: {
    //     type: Sequelize.UUID,
    //     references:{
    //     model: 'boards', // <<< Note, its table's name, not object name
    //     key: 'id' // <<< Note, its a column name
    //     }
    // }
    },
    { timestamps: false }
    );
   
    return lists;
  };