const dbConfig = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: false,

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.boards = require("./boards.model.js")(sequelize, Sequelize);
db.lists = require("./lists.model.js")(sequelize, Sequelize);
db.cards = require("./card.model.js")(sequelize, Sequelize);
db.checklists=require('./checklists.model.js')(sequelize, Sequelize);
db.checkitems=require('./checkitems.model.js')(sequelize, Sequelize);



db.boards.hasMany(db.lists,{ onDelete: 'cascade' });
db.lists.belongsTo(db.boards);

db.lists.hasMany(db.cards,{ onDelete: 'cascade' });
db.cards.belongsTo(db.lists);

db.boards.hasMany(db.cards);
// db.cards.belongsTo(db.boards);

db.boards.hasMany(db.checklists);


db.cards.hasMany(db.checklists,{ onDelete: 'cascade' });
db.checklists.belongsTo(db.cards);


db.checklists.hasMany(db.checkitems,{ onDelete: 'cascade' });
db.checkitems.belongsTo(db.checklists);



module.exports = db;

