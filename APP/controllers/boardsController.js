const db = require("../models/index");
const Boards = db.boards;
const Lists = db.lists;
const Op = db.Sequelize.Op;

// Create and Save a new Tutorial
exports.getAllBoards = async (req, res) => {
  
    try {
        let data= await Boards.findAll({
            include: [
            {model:db.cards},
            {model:db.lists},
            {model:db.checklists},
        
        ],
         })
         res.json(data);
       } catch (error) {
        res.json(error)
       }
};

exports.createABoard = async (req, res) => {
  
    try {
        // console.log(req.query.name)
        let data= await Boards.create({name:req.query.name})
        console.log(data)
         res.json(data);
          
       } catch (error) {

        res.json(error)
       }

};

exports.deleteABoard = async (req, res) => {
  
    try {
        await Boards.destroy({
            where: {
              id: req.body.id
            },
          });
          res.status(200).json("deleted");
       } catch (error) {

        res.json(error)
       }

};


exports.getAllCardsbyBoardId = async (req, res) => {
  
    try {
        let data= await Boards.findOne({
         
            where :[{
            id:req.params.id,
            }],
            include: [{model:db.cards}],
         })
         
         res.json(data.dataValues.cards);
       } catch (error) {
        res.status(500).json(error.message)
       }
};

exports.getAllListsbyBoardId = async (req, res) => {
  
    try {
        let data= await Boards.findOne({
            where :[{
            id:req.params.id,
            }],
            include: [{model:db.lists}],
         })
         res.json(data.dataValues.lists);
       } catch (error) {
        res.json(error)
       }
};

exports.getAllChecklistsbyBoardId = async (req, res) => {
  
    try {
        let data= await Boards.findOne({
            where :[{
            id:req.params.id,
            }],
            include: [{model:db.checklists ,include:[{model:db.checkitems}]}],
         })
         res.json(data.dataValues.checklists);
       } catch (error) {
        res.json(error)
       }
};