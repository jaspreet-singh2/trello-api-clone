module.exports = (sequelize, Sequelize) => {
  const checklists = sequelize.define(
    "checklists",
    {
      id: {
        // type: Sequelize.INTEGER,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
        primaryKey: true,
      },
      name: {
        type: Sequelize.STRING,
      },
    },
    { timestamps: false }
  );

  return checklists;
};
