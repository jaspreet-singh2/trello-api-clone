const db = require("../models/index");
const Lists = db.lists;
const Op = db.Sequelize.Op;

// Create and Save a new Tutorial
exports.getAllLists = async (req, res) => {
  
    try {
        let data= await Lists.findAll()
         res.json(data);
       } catch (error) {
        res.json(error)
       }
};


exports.createAList = async (req, res) => {
  
    try {
        let data= await Lists.create({name: req.query.name,boardId:req.query.idBoard})
         res.json(data);
         
       } catch (error) {

        res.json(error)
       }


};

exports.deleteAList = async (req, res) => {
  
  try {
    // console.log();
    await Lists.destroy({
      where: {
        id: req.params.id
      },
    });
    res.status(200).json("deleted");
     } catch (error) {

      res.json(error)
     }


};
