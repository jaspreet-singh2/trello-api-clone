const db = require("../models/index");
const Checkitems = db.checkitems;
const Op = db.Sequelize.Op;

// Create and Save a new Tutorial
exports.getAllCheckitems = async (req, res) => {
  
    try {
        let data= await Checkitems.findAll()
         res.json(data);
       } catch (error) {
        res.json(error)
       }

};

exports.createACheckitem = async (req, res) => {
  
    try {
         const query=req.query; 

        let data= await Checkitems.create({checklistId:query.checklistId, name:query.name });
         res.json(data);
         
       } catch (error) {

        res.json(error)
       }

       
};

exports.deleteACheckitem = async (req, res) => {
  
  try {
    await Checkitems.destroy({
      where: {
        id: req.query.id
      },
    });
    res.status(200).json("deleted");
     } catch (error) {

      res.status(500).json(error)
     }
};


exports.updateACheckitem = async (req, res) => {
  
  try {
    const checkitem= await Checkitems.findByPk(req.query.id)
    
     const ans=await  checkitem.update({state:req.query.state});

    res.status(200).json(ans);
     } catch (error) {

      res.status(500).json(error)
     }
};


