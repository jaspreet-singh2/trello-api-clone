const db = require("../models/index");
const Checklists = db.checklists;
const Op = db.Sequelize.Op;

// Create and Save a new Tutorial
exports.getAllChecklists = async (req, res) => {
  
    try {
        let data= await db.checklists.findAll()
         res.json(data);
       } catch (error) {
        res.json(error)
       }

};

exports.createAChecklist = async (req, res) => {
  
    try {
  
      let boardId= await db.cards.findByPk(req.query.cardId)
       let  checklist = {name:req.query.name,boardId: boardId.dataValues.boardId,cardId:req.query.cardId};
        let data= await db.checklists.create(checklist);
        data.dataValues.checkitems=[]
         res.json(data);
         
       } catch (error) {

        res.json(error)
       }

       
};

exports.deleteAChecklist = async (req, res) => {
  
  try {
    await Checklists.destroy({
      where: {
        id: req.query.id
      },
    });
    res.status(200).json("deleted");
     } catch (error) {

      res.status(500).json(error)
     }


};