const{createABoard,getAllBoards, deleteABoard, getAllListsbyBoardId, getAllCardsbyBoardId, getAllChecklistsbyBoardId}= require('../controllers/boardsController')


var routerBoards = require("express").Router();


routerBoards.get('/',getAllBoards);

routerBoards.post('/',createABoard);

routerBoards.delete('/',deleteABoard);

routerBoards.get('/:id/lists',getAllListsbyBoardId);

routerBoards.get('/:id/cards',getAllCardsbyBoardId);

routerBoards.get('/:id/checklists',getAllChecklistsbyBoardId);




module.exports=routerBoards;