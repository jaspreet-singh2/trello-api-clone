const{createAList,getAllLists, deleteAList}= require('../controllers/listsController')


var routerLists = require("express").Router();


routerLists.get('/',getAllLists);

routerLists.post('/',createAList);

routerLists.delete('/:id',deleteAList);


module.exports=routerLists;