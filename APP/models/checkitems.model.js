module.exports = (sequelize, Sequelize) => {
    const checkitems = sequelize.define(
      "checkitems",
      {
        id: {
          // type: Sequelize.INTEGER,
          type: Sequelize.UUID,
          defaultValue: Sequelize.UUIDV4,
          allowNull: false,
          primaryKey: true,
        },
        name: {
          type: Sequelize.STRING,
        },
        state :{
            type: Sequelize.STRING,
            defaultValue :'incomplete'
        }
      },
      { timestamps: false }
    );
  
    return checkitems;
  };
  