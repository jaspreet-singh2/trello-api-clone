// boardData.js

const boardData = [
    {
      name: 'Dreamy Island',
      //bgIMg: 'dreamy-island.jpg',
      bgcolor: 'skyblue',
    },
    {
      name: 'Space Odyssey',
      //bgIMg: 'space-odyssey.jpg',
      bgcolor: 'darkblue',
    },
    {
      name: 'Sunset Serenity',
      //bgIMg: 'sunset-serenity.jpg',
      bgcolor: 'orange',
    },
    {
      name: 'Forest Retreat',
      //bgIMg: 'forest-retreat.jpg',
      bgcolor: 'green',
    },
    {
      name: 'Underwater Paradise',
      //bgIMg: 'underwater-paradise.jpg',
      bgcolor: 'aqua',
    },
    {
      name: 'City Lights',
      //bgIMg: 'city-lights.jpg',
      bgcolor: 'purple',
    },
    {
      name: 'Mystic Mountains',
      //bgIMg: 'mystic-mountains.jpg',
      bgcolor: 'lavender',
    },
    {
      name: 'Desert Mirage',
      //bgIMg: 'desert-mirage.jpg',
      bgcolor: 'sand',
    },
    {
      name: 'Tropical Bliss',
      //bgIMg: 'tropical-bliss.jpg',
      bgcolor: 'turquoise',
    },
    {
      name: 'Enchanted Garden',
      //bgIMg: 'enchanted-garden.jpg',
      bgcolor: 'pink',
    },
  ];
  

  
  module.exports = {boardData,}
  