const { lists } = require(".");



module.exports = (sequelize, Sequelize) => {
    const cards = sequelize.define("cards", {
       
      id: {
        // type: Sequelize.INTEGER,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
        primaryKey: true
      },
      name: {
        type: Sequelize.STRING
      },
     
    
    
    },
    { timestamps: false }
    );
   
    return cards;
  };