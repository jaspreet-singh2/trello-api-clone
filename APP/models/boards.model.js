function getRandomColor() {
  // Generate random values for red, green, and blue components
  var red = Math.floor(Math.random() * 256);
  var green = Math.floor(Math.random() * 256);
  var blue = Math.floor(Math.random() * 256);

  // Construct a CSS color string in the format "rgb(red, green, blue)"
  var color = "rgb(" + red + "," + green + "," + blue + ")";

  return color;
}

// Example usage:
var randomColor = getRandomColor();
console.log(randomColor);

module.exports = (sequelize, Sequelize) => {
    const boards = sequelize.define("boards", {
       
      id: {
        // type: Sequelize.INTEGER,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
        primaryKey: true
      },
      name: {
        type: Sequelize.STRING

      },
      bgImg: {
        type: Sequelize.STRING,
        defaultValue: function(){return `https://source.unsplash.com/random/300x200?sig=${Math.random()}`}
      },
      bgcolor:
      {
        type: Sequelize.STRING,
        defaultValue: getRandomColor()
      },
    },
    { timestamps: false }
    );
  
    return boards;
  };