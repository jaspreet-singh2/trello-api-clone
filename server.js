const express = require("express");
// const bodyParser = require("body-parser");
const cors = require("cors");

const db = require("./APP/models/index");
const app = express();
const {boardData} = require("./boardData");
const routerBoards = require("./APP/routes/boards");
const routerLists = require("./APP/routes/lists");
const routerCards = require("./APP/routes/cards");
const routerChecklists = require("./APP/routes/checklist");
const routerCheckitems = require("./APP/routes/checkitems");


var corsOptions = {
  origin: "*",
};

async function initDB() {
  try {
    await db.sequelize.sync({  });
    //
    console.log("Drop and re-sync db.");

    //  await db.boards.bulkCreate(boardData);

    // // const randomBoardIndex = Math.floor(Math.random() * boardData.length);
    //  const data= await db.boards.findAll()
     
    //  console.log(data[0].dataValues.id);

    //  await db.lists.create({name :'new list 1',boardId:data[0].dataValues.id})
       
    //  const datalist = await db.lists.findAll()
    //  await db.cards.create({name :'new card 1',boardId:data[0].dataValues.id,listId :datalist[0].dataValues.id})
    //  const datacard = await db.cards.findAll()
    //  await db.checklists.create({name :'new card 1',boardId:data[0].dataValues.id,cardId :datacard[0].dataValues.id})
     
    //  const datacheklist = await db.checklists.findAll()

    //   const w=await db.checkitems.create({name:'check item 1' ,state:'inComplete',checklistId:datacheklist[0].dataValues.id })
    //   //  console.log(w);
    console.log("10 boards with creative names, colors, and backgrounds inserted successfully.");
  } catch (error) {
    console.log(error);
  }
}
initDB();

app.use(cors(corsOptions));

// parse requests of content-type - application/json
// app.use(bodyParser.json());
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
// app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.urlencoded());

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to bezkoder application." });
});

app.use("/boards",routerBoards);

app.use("/lists",routerLists);

app.use("/cards",routerCards);

app.use("/checklists",routerChecklists);

app.use("/checkitems",routerCheckitems);

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
