const{createACard,getAllCards, deleteACard}= require('../controllers/cardsController')


var routerCards = require("express").Router();


routerCards.get('/',getAllCards);

routerCards.post('/',createACard);

routerCards.delete('/:id',deleteACard);


module.exports=routerCards;