const{createACheckitem,getAllCheckitems, deleteACheckitem, updateACheckitem}= require('../controllers/checkitemController')


var routerCheckitems = require("express").Router();


routerCheckitems.get('/',getAllCheckitems);

routerCheckitems.post('/',createACheckitem);

routerCheckitems.delete('/',deleteACheckitem);

routerCheckitems.put('/',updateACheckitem);
module.exports=routerCheckitems;