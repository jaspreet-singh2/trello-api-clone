const db = require("../models/index");
const Cards = db.cards;
const Op = db.Sequelize.Op;

// Create and Save a new Tutorial
exports.getAllCards = async (req, res) => {
  
    try {
        let data= await Cards.findAll()
         res.json(data);
       } catch (error) {
        res.json(error)
       }

};



exports.createACard = async (req, res) => {
  
    try {
      
       let boardId= await db.lists.findByPk(req.query.idList)
       let  card= {name:req.query.name,listId:req.query.idList,boardId: boardId.dataValues.boardId};
        let data= await Cards.create(card);
         res.json(data);
         
       } catch (error) {

        res.json(error)
       }

       
};


exports.deleteACard = async (req, res) => {
  
  try {
    await Cards.destroy({
      where: {
        id: req.params.id
      },
    });
    res.status(200).json("deleted");
     } catch (error) {

      res.status(500).json(error)
     }


};
